<div align="center">
<img src="https://user-images.githubusercontent.com/71400605/245153253-51ec8358-41f3-4a07-929f-52f3057c2c58.jpeg" alt="Pranjal Kumar">
<br>
<h1>Hi, I'm Pranjal Kumar  
<img src="./res/dancing-kitty-kwaii.gif" width="70"></h1>
<h3 align="center">I am 3rd-year undergrad at CGU Odisha pursuing my B.Tech in Computer Science & Engineering. I am an aspiring Developer. I am currently looking for internship opportunities in SDE role. I love Problem Solving and I have solved more than 500+ problems on various coding platforms.</h3>

<!-- Typing Effect -->
[![Typing SVG](https://readme-typing-svg.herokuapp.com?color=8ccf72&center=true&lines=Web+Developer;Competitive+Programmer;Tech+Enthusiast;Open+Source+Developer;Finance+Geek;Machine-Learning+Enthusiast)](https://git.io/typing-svg)


<!-- Stats -->
<a href="http://www.github.com/pranjal-barnwal"><img src="https://github-readme-stats.vercel.app/api?username=pranjal-barnwal&show_icons=true&hide=&count_private=true&title_color=8ccf72&text_color=ffffff&icon_color=8cd073&bg_color=000000&hide_border=true&show_icons=true" alt="pranjal-barnwal's GitHub stats" /></a>
<a href="http://www.github.com/pranjal-barnwal"><img src="https://github-readme-streak-stats.herokuapp.com/?user=pranjal-barnwal&stroke=ffffff&background=000000&ring=8ccf72&fire=fa8b00&currStreakNum=ffffff&currStreakLabel=8cd073&sideNums=ffffff&sideLabels=ffffff&dates=ffffff&hide_border=true" /></a>

<hr>

<h2 align="center">Connect with me: <img src="https://media.giphy.com/media/mGcNjsfWAjY5AEZNw6/giphy.gif" width="60"></h2>
<p align="center">
<a href="https://www.linkedin.com/in/pranjal-barnwal"><img src="https://img.shields.io/badge/-Pranjal_Kumar-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/pranjal-barnwal/" /></a>
<a href="mailto:itselfpranjalkr@gmail.com"><img src="https://img.shields.io/badge/-itselfpranjalkr@gmail.com-ea4335?style=flat-square&logo=Gmail&logoColor=white&link=mailto:itselfpranjalkr@gmail.com" /></a>
<!-- <a href="https://twitter.com/pranjalBarnwal_"><img src="https://img.shields.io/badge/-pranjalBarnwal_-219df3?style=flat-square&logo=twitter&logoColor=white&link=https://twitter.com/pranjalBarnwal_" /></a> -->
<a href="https://www.figma.com/@pranjal_barnwal"><img src="https://img.shields.io/badge/-pranjal_barnwal_-09c47c?style=flat-square&logo=figma&logoColor=white&link=https://www.figma.com/@pranjal_barnwal" /></a>
<a href="https://instagram.com/pranjal_barnwal_"><img src="https://img.shields.io/badge/-pranjal_barnwal_-f77880?style=flat-square&logo=instagram&logoColor=white&link=https://instagram.com/pranjal_barnwal_" /></a>
</p>
<br>

<hr>

![Snake animation](https://github.com/pranjal-barnwal/pranjal-barnwal/blob/output/github-contribution-grid-snake.svg)

<br>
<p align="center"> 

</p>


</div>
